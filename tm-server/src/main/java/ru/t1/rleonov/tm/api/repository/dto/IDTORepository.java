package ru.t1.rleonov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;
import java.util.Collection;

public interface IDTORepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void set(@NotNull Collection<M> models);

    void update(@NotNull M model);

    void remove(@NotNull M model);

}
